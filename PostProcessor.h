    #pragma once
#include <QtCore>
#include <QString>
#include "opencv2/core/core.hpp"
#include "recognitioninfo.h"
#include "postprocessingsettings.h"
#include "utils.h"
#include <QMap>

class PostProcessor :
    public QObject
{
    Q_OBJECT
public:
    PostProcessor(QObject * parent = 0);
    ~PostProcessor();

    void processFrame(cv::Mat * frame, PostProcessingSettings *settings);

public slots :
    void newData(std::vector<cv::Rect> facesRects, QList<RecognitionInfo> facesLabels);

signals:
    void statusMessage(QString message, int timeout = 0);

private:

    QList<RecognitionInfo>  recognitionInfos;
    std::vector<cv::Rect>   mFacesRects;
    QMap<QString, int>   lastSaveTimeMap;

    bool canSaveFrame(QString label, int minInterval);

};
