#ifndef FRAMEPROCESSOR_H
#define FRAMEPROCESSOR_H

#include <QObject>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"


class SearchFacesProcessor : public QObject
{
    Q_OBJECT
private:
    cv::Mat                 frame;
    int                     workerNumber;
    QString                 classifierPath;
    cv::CascadeClassifier*  classifier;

public:
    SearchFacesProcessor(QString path, int number = 0, QObject *parent = 0);
public slots:
    void startProcessing();
    void findFaces(cv::Mat frame, int workerNumber);
signals:
    void facesFound(std::vector<cv::Rect> faces, QList<cv::Mat> facesFrames);
    void finished(int duration, int number);
};

#endif // FRAMEPROCESSOR_H
