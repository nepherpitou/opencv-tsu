#include "postprocessingsettings.h"
#include <QSettings>
#include <QDir>

PostProcessingSettings::PostProcessingSettings(QObject *parent) : QObject(parent),
    saveRecognizedFaces(false),
    saveUnrecognizedFaces(false),
    recognizedPath(""),
    unrecognizedPath("")
{

}

PostProcessingSettings::~PostProcessingSettings()
{

}

void PostProcessingSettings::setRecognizedFacesPolicy(bool save, QString path, int interval)
{
    this->saveRecognizedFaces = save;
    this->recognizedPath = path.length() == 0 ? this->recognizedPath : path;
    this->minimumRecSaveInterval = interval;
}

void PostProcessingSettings::setUnrecognizedFacesPolicy(bool save, QString path, int interval)
{
    this->saveUnrecognizedFaces = save;
    this->unrecognizedPath = path.length() == 0 ? this->unrecognizedPath : path;
    this->minimumUnrecSaveInterval = interval;
}

void PostProcessingSettings::setThreshold(int threshold)
{
    this->threshold = threshold;
}

bool PostProcessingSettings::saveRecognized()
{
    return this->saveRecognizedFaces;
}

bool PostProcessingSettings::saveUnrecognized()
{
    return this->saveUnrecognizedFaces;
}

QString PostProcessingSettings::getRecognizedPath()
{
    return this->recognizedPath;
}

QString PostProcessingSettings::getUnrecognizedPath()
{
    return this->unrecognizedPath;
}

int PostProcessingSettings::getRecInterval()
{
    return minimumRecSaveInterval;
}

int PostProcessingSettings::getUnrecInterval()
{
    return minimumUnrecSaveInterval;
}

int PostProcessingSettings::getThreshold()
{
    return threshold;
}

bool PostProcessingSettings::loadSettingsFromFile(QString file)
{
    if(QFile(file).exists())
    {
        QSettings settings(file, QSettings::IniFormat, this);
        this->saveRecognizedFaces = settings.value(SETTINGS_SAVE_RECOGNIZED, this->saveRecognizedFaces).toBool();
        this->saveUnrecognizedFaces = settings.value(SETTINGS_SAVE_UNRECOGNIZED, this->saveUnrecognizedFaces).toBool();
        this->recognizedPath = settings.value(SETTINGS_RECOGNIZED_PATH, this->recognizedPath).toString();
        this->unrecognizedPath = settings.value(SETTINGS_UNRECOGNIZED_PATH, this->unrecognizedPath).toString();
        this->minimumRecSaveInterval = settings.value(SETTINGS_RECOGNIZED_INTERVAL, this->minimumRecSaveInterval).toInt();
        this->minimumUnrecSaveInterval = settings.value(SETTINGS_UNRECOGNIZED_INTERVAL, this->minimumUnrecSaveInterval).toInt();
        this->threshold = settings.value(SETTINGS_THRESHOLD, this->threshold).toInt();
        return true;
    } else
    {
        return false;
    }
}

bool PostProcessingSettings::saveSettingsToFile(QString file)
{
    QSettings settings(file, QSettings::IniFormat, this);
    settings.setValue(SETTINGS_SAVE_RECOGNIZED, this->saveRecognizedFaces);
    settings.setValue(SETTINGS_SAVE_UNRECOGNIZED, this->saveUnrecognizedFaces);
    settings.setValue(SETTINGS_RECOGNIZED_PATH, this->recognizedPath);
    settings.setValue(SETTINGS_UNRECOGNIZED_PATH, this->unrecognizedPath);
    settings.setValue(SETTINGS_RECOGNIZED_INTERVAL, this->minimumRecSaveInterval);
    settings.setValue(SETTINGS_UNRECOGNIZED_INTERVAL, this->minimumUnrecSaveInterval);
    settings.setValue(SETTINGS_THRESHOLD, this->threshold);
    return true;
}

