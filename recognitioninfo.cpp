#include "recognitioninfo.h"

RecognitionInfo::RecognitionInfo() :
    label(""),
    confidence(0),
    time(QTime::currentTime()),
    face(),
    recognized(false)
{

}

RecognitionInfo::~RecognitionInfo()
{
    face.release();
}

