#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "opencv2/objdetect/objdetect.hpp"
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    cameraCapture(0),
    reusableImage(0),
    frameTimer(),
    searchFacesManager(0),
    mFaceRecognizer(0),
    postProcessor(0),
    processingSettings(new PostProcessingSettings(this))
{
    ui->setupUi(this);
    this->currentWorkMode = WorkMode(ui->sourceSelect->currentIndex());
    frameTimer.setInterval(41);
    qRegisterMetaType<cv::Mat>("cv::Mat");
    qRegisterMetaType< std::vector<cv::Rect> >("std::vector<cv::Rect>");
    qRegisterMetaType< std::vector<cv::Mat> >("std::vector<cv::Mat>");
    qRegisterMetaType< QList<cv::Mat> >("QList<cv::Mat>");
    cv::setNumThreads(1);
    connect(this->ui->buttonSaveTrainData, SIGNAL(clicked()), this, SLOT(onSaveTrainDataClicked()));
    connect(this->ui->buttonLoadTrainData, SIGNAL(clicked()), this, SLOT(onLoadTrainDataClicked()));
    connect(this->ui->buttonSelectRecognizedPath, SIGNAL(clicked()), this, SLOT(onSelectRecognizedPathClicked()));
    connect(this->ui->buttonSelectUnrecognizedPath, SIGNAL(clicked()), this, SLOT(onSelectUnrecognizedPathClicked()));
    connect(this->ui->buttonResetProcSettings, SIGNAL(clicked()), this, SLOT(onResetProcSettings()));
    connect(this->ui->buttonSetProcSettings, SIGNAL(clicked()), this, SLOT(onSetProcSettings()));
    // Связываем таймауты таймера с функцией получения следующего кадра
    connect(&this->frameTimer, SIGNAL(timeout()), this, SLOT(processFrame()));
    this->mFaceRecognizer = new FaceRecognizerManager();
    loadSettingsFromFile("settings.ini");
}


MainWindow::~MainWindow()
{
    onStopClicked();
    saveSettingsToFile("settings.ini");
    delete ui;
}


void MainWindow::onStartClicked()
{

    if(this->frameTimer.isActive()){
        QMessageBox::warning(this, tr("Warning!"), tr("Already running. Please stop first."));
        return;
    }

    switch(this->currentWorkMode)
    {
    case CAMERA:
    {
        // Инициализация камеры для поска и идентификации лиц
        this->cameraCapture = new cv::VideoCapture(CV_CAP_ANY);
        if(!this->cameraCapture->isOpened() || !this->cameraCapture->grab()){
            QMessageBox::critical(this, QString("Cannot open camera!"), QString("Device open error. Maybe device is busy."));
            return;
        }
        break;
    }
    case VIDEO:
    {
        // Загрузка видео для поска и идентификации лиц
        QString filename = QFileDialog::getOpenFileName(this, QString("Select video source"));
        if(filename.length() > 0)
            this->cameraCapture = new cv::VideoCapture(filename.toStdString());
        if(!this->cameraCapture->isOpened() || !this->cameraCapture->grab()){
            QMessageBox::critical(this, QString("Cannot open camera!"), QString("File open error. Maybe file has wrong or unsupported format."));
            onStopClicked();
            return;
        }
        break;
    }
    case PICTURE:
    {
        // Загрузка изображения для поска и идентификации лиц
        QString filename = QFileDialog::getOpenFileName(this, QString("Select picture source"));
        if(filename.length() > 0)
            this->reusableImage = new cv::Mat(cv::imread(filename.toStdString()));
        break;
    }
    }

    //Загрузка файла классификатора
    QString classifierFile;
    if(QFile(this->pathCascadeClassifierFile).exists())
    {
        classifierFile = this->pathCascadeClassifierFile;
    }else
    {
        QMessageBox::critical(this, tr("Critical"), tr("Classifier cascade not loaded. Can't initialize search engine"));
        onStopClicked();
        return;
    }

    // Инициализируем контроллер поиска лиц
    if(!this->searchFacesManager)
        this->searchFacesManager = new SearchFacesManager();
    this->searchFacesManager->setFaceRecognizer(this->mFaceRecognizer);

    // Инициализируем постпроцессор.
    if (!this->postProcessor){
        this->postProcessor = new PostProcessor();
        connect(this->postProcessor, SIGNAL(statusMessage(QString, int)), ui->statusBar, SLOT(showMessage(QString, int)));
    }

    // Организуем цепочку обработки кадра (лица опознаны -> результаты опознания отображены)
    connect(
                this->searchFacesManager,
                SIGNAL(onFacesRecognizeFinished(std::vector<cv::Rect>, QList<RecognitionInfo>)),
                this->postProcessor,
                SLOT(newData(std::vector<cv::Rect>, QList<RecognitionInfo>))
                );


    // Собственно запуск обработки потока с указанным файлом классификатора
    if(this->searchFacesManager->startProcessing(classifierFile)){
        connect(this, SIGNAL(onNeedFrameProcess(cv::Mat)), this->searchFacesManager, SLOT(processFrame(cv::Mat)));
    }else{
        QMessageBox::critical(this, tr("Critical!"), tr("Can't start faces search manager."));
    }

    // Запуск главного таймера захвата кадров
    this->frameTimer.start();
}

void MainWindow::processFrame()
{
    if(this->reusableImage == 0){
        this->reusableImage = new cv::Mat();
    }
    switch(this->currentWorkMode){
    case CAMERA:
        this->cameraCapture->read(*this->reusableImage);
        break;
    case VIDEO:
        this->cameraCapture->read(*this->reusableImage);
        break;
    case PICTURE:
        break;
    }
    emit onNeedFrameProcess(*this->reusableImage);
    this->outputImage = new cv::Mat(this->reusableImage->clone());
    if (this->searchFacesManager){
        for (cv::Mat face : this->searchFacesManager->getLastFoundFacesFrames()){
            emit onNeedFacePredict(cv::Mat(face));
        }
        this->postProcessor->processFrame(this->outputImage, this->processingSettings);
    }
    QPixmap outputPixmap = QPixmap::fromImage(Utils::mat2qimage(*this->outputImage));
    this->outputImage->release();
    ui->lbFrame->setPixmap(outputPixmap.scaled(ui->lbFrame->size()));
}

void MainWindow::onStopClicked()
{
    if(frameTimer.isActive())
        frameTimer.stop();
    if(cameraCapture != 0 && cameraCapture->isOpened())
        cameraCapture->release();
    if(this->searchFacesManager && this->searchFacesManager->isReady()){
        this->searchFacesManager->stopProcessing();
        disconnect(this->searchFacesManager);
    }
}

void MainWindow::onSourceChanged(int newSource)
{
    this->currentWorkMode = WorkMode(newSource);
    onStopClicked();
}

void MainWindow::onAddSampleClicked()
{
    QString newPath = QFileDialog::getExistingDirectory(this, trUtf8("Select folder with faces samples"));
    if(!newPath.isEmpty())
    {
        this->mFaceRecognizer->addPath(newPath);
        this->syncRecognitionTab();
    }
}

void MainWindow::syncRecognitionTab()
{
    this->ui->tableSamplesSet->clearContents();
    this->ui->tableSamplesSet->setRowCount(this->mFaceRecognizer->getSamplePaths()->length());
    this->ui->tableSamplesSet->setColumnCount(2);
    QTableWidget *samplesTable = this->ui->tableSamplesSet;
    QHeaderView *header = samplesTable->horizontalHeader();
    header->setSectionResizeMode(QHeaderView::ResizeMode::Stretch);
    for(int i = 0; i < this->mFaceRecognizer->getSamplePaths()->length(); i++){
        QString path = this->mFaceRecognizer->getSamplePaths()->at(i);
        QString label = this->mFaceRecognizer->getLabels()->at(i);
        QTableWidgetItem *samplePath = new QTableWidgetItem(tr("%1").arg(path));
        samplePath->setFlags(samplePath->flags() ^ Qt::ItemIsEditable);
        QTableWidgetItem *sampleLabel = new QTableWidgetItem(tr("%1").arg(label));
        samplesTable->setItem(i, 0, samplePath);
        samplesTable->setItem(i, 1, sampleLabel);
    }
}

void MainWindow::saveSettingsToFile(QString fileName)
{
    QSettings settings(fileName, QSettings::IniFormat, this);
    settings.setValue(SETTINGS_CASCADE_PATH, this->pathCascadeClassifierFile);
    this->processingSettings->saveSettingsToFile(fileName);
    ui->statusBar->showMessage(trUtf8("Настройки сохранены!"));
}

void MainWindow::loadSettingsFromFile(QString fileName)
{
    QSettings settings(fileName, QSettings::IniFormat, this);
    this->pathCascadeClassifierFile = settings.value(SETTINGS_CASCADE_PATH).toString();
    this->processingSettings->loadSettingsFromFile(fileName);
    syncSearchTab();
    syncProcessingTab(false);
}

void MainWindow::syncSearchTab()
{
    this->ui->editCascadeFile->setText(this->pathCascadeClassifierFile);
}

void MainWindow::syncProcessingTab(bool fromUi)
{
    if(fromUi)
    {
        QString recPath         = ui->editRecognizedPath->text();
        QString unrecPath       = ui->editUnrecognizedPath->text();
        int     recThreshold    = ui->editRecThreshold->value();
        int     recInterval     = ui->editRecFaceInterval->value();
        int     unrecInterval   = ui->editUnrecFaceInterval->value();
        bool    saveRec         = ui->cbSaveRecognizedFaces->isChecked();
        bool    saveUnrec       = ui->cbSaveUnrecognizedFaces->isChecked();
        processingSettings->setRecognizedFacesPolicy(saveRec, recPath, recInterval);
        processingSettings->setUnrecognizedFacesPolicy(saveUnrec, unrecPath, unrecInterval);
        processingSettings->setThreshold(recThreshold);
        saveSettingsToFile("settings.ini");
    } else
    {
        ui->cbSaveRecognizedFaces->setChecked(this->processingSettings->saveRecognized());
        ui->cbSaveUnrecognizedFaces->setChecked(this->processingSettings->saveUnrecognized());
        ui->editRecognizedPath->setText(this->processingSettings->getRecognizedPath());
        ui->editUnrecognizedPath->setText(this->processingSettings->getUnrecognizedPath());
        ui->editRecFaceInterval->setValue(processingSettings->getRecInterval());
        ui->editUnrecFaceInterval->setValue(processingSettings->getUnrecInterval());
        ui->editRecThreshold->setValue(processingSettings->getThreshold());
    }
}

void MainWindow::onSamplesTableCellChanged(int row, int col)
{
    if(col == 1){
        QTableWidgetItem *item = this->ui->tableSamplesSet->item(row, col);
        QString newLabel = item->text();
        this->mFaceRecognizer->setLabelForPath(row, newLabel);
    }
}

void MainWindow::onSaveTrainDataClicked()
{
    if(this->mFaceRecognizer == NULL)
    {
        this->mFaceRecognizer = new FaceRecognizerManager();
    }
    if(!this->mFaceRecognizer->isTrained()){
        if(!this->mFaceRecognizer->trainOnSamples())
        {
            QMessageBox::critical(this, trUtf8("Error!"), trUtf8("Нечего сохранять!"));
            return;
        }
    }
    QString fileName = QFileDialog::getSaveFileName(this, trUtf8("Куда сохранить настройки?"));
    if(fileName.endsWith(".ini")){
        fileName = fileName.left(fileName.length() - 4);
    }
    this->mFaceRecognizer->getFaceRecognizer()->save(QString(fileName + ".dat").toStdString());
    QSettings settings(fileName + ".ini", QSettings::IniFormat, this);
    int labelsCount = this->mFaceRecognizer->getSamplePaths()->size();
    settings.setValue("COUNT", labelsCount);
    for(int i = 0; i < labelsCount; i++)
    {
        settings.setValue(QString("LABEL_%1").arg(i), this->mFaceRecognizer->getNameForLabel(i));
        settings.setValue(QString("PATH_%1").arg(i), this->mFaceRecognizer->getSamplePaths()->at(i));
    }
    settings.sync();
}

void MainWindow::onLoadTrainDataClicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Укажите сохраненный классификатор"), QString(), QString("*.ini"));
    QSettings settings(fileName, QSettings::IniFormat, this);
    int count = settings.value("COUNT", 0).toInt();
    if(count == 0)
    {
        QMessageBox::critical(this, tr("Error"), tr("Не удалось прочитать настройки"));
        return;
    }
    QString recogSettings = fileName.replace(fileName.length() - 3, 3, "dat");
    this->mFaceRecognizer->trainFromFile(recogSettings);
    for(int i = 0; i < count; i++)
    {
        QString label = settings.value(QString("LABEL_%1").arg(i), QString::number(i)).toString();
        QString path = settings.value(QString("PATH_%1").arg(i), QString::number(i)).toString();
        this->mFaceRecognizer->addPath(path);
        this->mFaceRecognizer->setLabelForPath(i, label);
    }
    this->syncRecognitionTab();
}

void MainWindow::onSelectCascadeClicked()
{
    this->pathCascadeClassifierFile = QFileDialog::getOpenFileName(this, tr("Select cascade classifier file"));
    syncSearchTab();
}

void MainWindow::onSelectRecognizedPathClicked()
{
    QString path = QFileDialog::getExistingDirectory(this, trUtf8("Укажите корневой каталог для распознанных лиц"));
    ui->editRecognizedPath->setText(path);
}

void MainWindow::onSelectUnrecognizedPathClicked()
{
    QString path = QFileDialog::getExistingDirectory(this, trUtf8("Укажите корневой каталог для нераспознанных лиц"));
    ui->editUnrecognizedPath->setText(path);
}

void MainWindow::onResetProcSettings()
{
    syncProcessingTab(false);
}

void MainWindow::onSetProcSettings()
{
    syncProcessingTab(true);
}


void MainWindow::onTrainSamplesClicked()
{
    this->mFaceRecognizer->trainOnSamples();
}


