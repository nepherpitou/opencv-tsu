#ifndef SEARCHFACESMANAGER_H
#define SEARCHFACESMANAGER_H

#include <QObject>
#include <QThread>
#include <QVector>
#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "searchfacesprocessor.h"
#include "FaceDetectorManager.h"

class SearchFacesManager : public QObject
{
    Q_OBJECT
private:
    std::vector<cv::Rect>           lastFoundFaces;
    QList<cv::Mat>                  lastFoundFacesFrames;
    QVector<int>                    freeWorkers;
    bool                            ready;
    int                             threadsCount;
    int                             maxThreadsCount;
    qint64                          lastFacesFoundTime;
    QString                         haarCascadePath;

    FaceRecognizerManager*            mFaceRecognizer;
    
    

public:
    explicit SearchFacesManager(int maxThreads = 1, QObject *parent = 0);

    bool startProcessing(QString haarClassifierPath);
    void stopProcessing();
    void setFaceRecognizer(FaceRecognizerManager * faceRecognizer);

    inline bool isReady(){ return this->ready;}
    QList<cv::Mat> getLastFoundFacesFrames();

public slots:
    void processFrame(cv::Mat frame);
    void facesFound(std::vector<cv::Rect> faces, QList<cv::Mat> facesFrames);
    void workerFinishWork(int elapsedTime, int workerNumber);

signals:
    void stopAll();
    void sendTask(cv::Mat frame, int workerNumber);
    void onFacesRecognizeFinished(std::vector<cv::Rect> facesRects, QList<RecognitionInfo> facesInfo);

};

#endif // SEARCHFACESMANAGER_H
