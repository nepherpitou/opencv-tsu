#include "PostProcessor.h"

PostProcessor::PostProcessor(QObject * parent) :
    QObject(parent),
    recognitionInfos(),
    mFacesRects(),
    lastSaveTimeMap()
{
}

PostProcessor::~PostProcessor()
{
    mFacesRects.clear();
    recognitionInfos.clear();
}

void PostProcessor::newData(std::vector<cv::Rect> facesRects, QList<RecognitionInfo> infos)
{
    this->mFacesRects = facesRects;
    this->recognitionInfos.clear();
    this->recognitionInfos.append(infos);
}

bool PostProcessor::canSaveFrame(QString label, int minInterval)
{
    int secFromLastSave = lastSaveTimeMap.contains(label) ?
                (QTime::currentTime().msecsSinceStartOfDay() - lastSaveTimeMap.value(label)) :
                -1;
    return secFromLastSave < 0 || secFromLastSave > minInterval;

}

void PostProcessor::processFrame(cv::Mat * frame, PostProcessingSettings* settings)
{
    for (uint i = 0; i < mFacesRects.size(); i++){
        cv::rectangle(*frame, mFacesRects.at(i), cv::Scalar(255, 0, 0), 3);
        RecognitionInfo info = recognitionInfos.at(i);
        info.recognized = info.confidence < settings->getThreshold() && info.confidence >=0;
        cv::putText(
                    *frame,
                    QString("%1 %2 %3").arg(info.label).arg(info.confidence).arg(info.recognized ? "REC" : "UNREC").toStdString(),
                    cv::Point(mFacesRects.at(i).x, mFacesRects.at(i).y),
                    cv::FONT_ITALIC,
                    1,
                    info.recognized ? cv::Scalar(0, 255, 0) : cv::Scalar(0, 0, 255),
                    3);
        bool canSaveRec = canSaveFrame(info.label, settings->getRecInterval());
        bool canSaveUnrec = canSaveFrame(info.label, settings->getUnrecInterval());
        if(
                info.recognized &&
                settings->saveRecognized() &&
                canSaveRec &&
                info.face.rows > 0 &&
                info.face.cols > 0)
        {
            QString filename = QString("%1_%2.jpg").arg(info.label).arg(info.time.toString("hh_mm_ss_zzz"));
            QDir dir = QDir(settings->getRecognizedPath() + QDir::separator() + info.label);
            if(dir.mkpath(dir.absolutePath()) || dir.exists())
            {
                Utils::mat2qimage(info.face).save(dir.absolutePath() + QDir::separator() + filename);
                emit statusMessage(trUtf8("Изображение %1 сохранено!").arg(filename), 0);
                lastSaveTimeMap[info.label] = info.time.msecsSinceStartOfDay();
            }
        }
        if(
                !info.recognized &&
                settings->saveUnrecognized() &&
                canSaveUnrec &&
                info.face.rows > 0 &&
                info.face.cols > 0)
        {
            QString filename = QString("%1_%2.jpg").arg("UNKNOWN").arg(info.time.toString("hh_mm_ss_zzz"));
            QDir dir = QDir(settings->getUnrecognizedPath());
            if(dir.mkpath(dir.absolutePath()) || dir.exists())
            {
                Utils::mat2qimage(info.face).save(dir.absolutePath() + QDir::separator() + filename);
                emit statusMessage(trUtf8("Изображение %1 сохранено!").arg(filename), 0);
                lastSaveTimeMap[info.label] = info.time.msecsSinceStartOfDay();
            }
        }
    }

}
