#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QThread>
#include <QSettings>

#include "utils.h"
#include "searchfacesmanager.h"
#include "FaceDetectorManager.h"
#include "PostProcessor.h"
#include "postprocessingsettings.h"
#include "recognitioninfo.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:
    void syncRecognitionTab();

    Ui::MainWindow  *ui;

    // Current work mode

    enum WorkMode{
        CAMERA = 0, VIDEO = 1, PICTURE = 2
    };

    WorkMode currentWorkMode;

    //OpenCV Capture interface
    cv::VideoCapture*   cameraCapture;
    cv::Mat*            reusableImage;
    cv::Mat*            outputImage;


    QTimer                          frameTimer;

    //Processors chain
    SearchFacesManager*             searchFacesManager;
    FaceRecognizerManager*          mFaceRecognizer;
    PostProcessor*                  postProcessor;

    // Settings data
    QString                         pathCascadeClassifierFile;
    bool                            saveUnrecognizedFaces = true;
    bool                            saveRecognizedFaces = true;
    PostProcessingSettings*         processingSettings;

    const QString SETTINGS_CASCADE_PATH = "haar_cascade_file_path";

    void saveSettingsToFile(QString fileName);
    void loadSettingsFromFile(QString fileName);


    // Interface updates
    void syncSearchTab();

    void syncProcessingTab(bool fromUi);


private slots:
    void onStartClicked();
    void processFrame();

    void onStopClicked();
    void onSourceChanged(int newSource);

    // Face recognition slots
    void onAddSampleClicked();
    void onTrainSamplesClicked();
    void onSamplesTableCellChanged(int row, int col);
    void onSaveTrainDataClicked();
    void onLoadTrainDataClicked();

    //Face detect slots
    void onSelectCascadeClicked();

    //Processing settings edit
    void onSelectRecognizedPathClicked();
    void onSelectUnrecognizedPathClicked();
    void onResetProcSettings();
    void onSetProcSettings();


signals:
    void onNeedFrameProcess(cv::Mat frame);
    void onNeedFacePredict(cv::Mat face);
};


#endif // MAINWINDOW_H
