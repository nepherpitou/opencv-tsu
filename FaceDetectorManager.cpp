#include "FaceDetectorManager.h"
#include <QDir>
#include <QDebug>


FaceRecognizerManager::FaceRecognizerManager() :
    trained(false)
{
}


FaceRecognizerManager::~FaceRecognizerManager()
{
}

/*
 * Добавление каталога с изображениями образцов в перечень
 * Если каталог отсутствует в списке, то он доавбялется в список, а его название добавляется в
 * sampleLabels.
 */
void FaceRecognizerManager::addPath(QString path)
{
    if (!this->samplePaths.contains(path)){
        this->samplePaths.append(path);
        QString label = path.split("/").last();
        this->sampleLabels.append(label);
    }
}

/*
 * Возвращает список каталогов наборов образцов.
 */
QList<QString>* FaceRecognizerManager::getSamplePaths()
{
    return &this->samplePaths;
}

/*
 * Возвращает список пользовательских наборов образцов.
 */
QList<QString> *FaceRecognizerManager::getLabels()
{
    return &this->sampleLabels;
}

/*
 * Устанавливает пользовательское названий набору образцов.
 */
void FaceRecognizerManager::setLabelForPath(int pathPosition, QString label)
{
    if(pathPosition < this->samplePaths.size() && pathPosition >= 0){
        this->sampleLabels.replace(pathPosition, label);
    }
}

/*
 * Метод построения модели по наборам образцов из указанных каталогов.
 */
bool FaceRecognizerManager::trainOnSamples()
{
    trained = false;
    this->mFaceRecognizer = cv::createFisherFaceRecognizer(50);
    std::vector<cv::Mat> trainSamples;
    std::vector<int> trainLabels;
    // Из каждого каталога загружается список подготовленных изображений, формируется список индентификаторов каталогов.
    for (QString path : this->samplePaths){
        std::vector<cv::Mat> samples = loadImagesFromPath(path);
        for (cv::Mat sample : samples){
            trainSamples.push_back(sample);
            trainLabels.push_back(this->samplePaths.indexOf(path));
        }
    }
    // Если списки, сформированные на предыдущем этапе не пустые, то данные передаются на обработку объекту mFaceRecognizer
    // и на их основе создаются модели для распознавания лиц.
    if (trainSamples.size() > 0)
    {
        this->mFaceRecognizer->train(trainSamples, trainLabels);
        trained = true;
    }
    return trained;
}

/*
 * Загружает сохраненные ранее модели из файла
 */
bool FaceRecognizerManager::trainFromFile(QString fileName)
{
    this->mFaceRecognizer = cv::createFisherFaceRecognizer(50);
    this->mFaceRecognizer->load(fileName.toStdString());
    return true;
}

/*
 * Производит загрузку и предобработку изображений-образцов из каталога.
 */
std::vector<cv::Mat> FaceRecognizerManager::loadImagesFromPath(QString path, int height, int width)
{
    QDir folder;
    folder.setPath(path);

    std::vector<cv::Mat> samples;

    for (QString filename : folder.entryList()){
        if (filename.endsWith(".jpg") || filename.endsWith(".png")){
            std::string fname = (path + QDir::separator() + filename).toStdString();
            cv::Mat sample = cv::imread(fname);
            // Изображения переводятся в оттенки серого
            cv::cvtColor(sample, sample, cv::COLOR_BGR2GRAY);
            // И масштабируются к единому размеру.
            cv::resize(sample, sample, cv::Size(width, height));
            samples.push_back(sample);
        }
    }
    return samples;
}

/*
 * Метод непосредтсвенно распознавания лица по изображению.
 */
const RecognitionInfo FaceRecognizerManager::recognizeFace(cv::Mat face)
{
    RecognitionInfo info;
    int result = -1;
    double confidence = 0;
    info.face = face;
    info.confidence = -1;
    if (this->mFaceRecognizer != NULL){
        // Изображение масштабируется к единому размеру, по которому подготавливались модели
        cv::resize(face, face, cv::Size(100, 100));
        // Вызывается метод predict, реализующий алгоритм идентификации лиц (в данном случае модификация метода главных компонент Fisherfaces
        mFaceRecognizer->predict(face, result, confidence);
        // Заполняется данными о результатах структура RecognitionInfo, которая ялвется результатом работы этой функции
        info.confidence = confidence;
        info.label = getNameForLabel(result);
        qDebug() << "Face predicted:" << result << "; Conf: " << confidence << "\n";
    }
    return info;
}

/*
 * Сохраняет подготовленные данные моделей для последующего использования без каталога образцов.
 */
void FaceRecognizerManager::saveTrainDataToFile(QString fileName)
{
    if(this->mFaceRecognizer != NULL)
    {
        this->mFaceRecognizer->save(fileName.toStdString());
    }
}

/*
 * Вовзращает объект FaceRecognizer
 */
cv::Ptr<cv::FaceRecognizer> FaceRecognizerManager::getFaceRecognizer()
{
    return this->mFaceRecognizer;
}

/*
 * Возвращает пользовательское названия, соответствующее идентификатору каталога образцов
 */
QString FaceRecognizerManager::getNameForLabel(int label){
    return this->samplePaths.size() > label && label >= 0 ? this->sampleLabels.at(label) : QString::number(label);
}

/*
 * Возвращает статус готовности к распознаванию
 */
bool FaceRecognizerManager::isTrained()
{
    return trained;
}
