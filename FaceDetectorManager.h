#pragma once
#include <QtCore>
#include <QList>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "recognitioninfo.h"

class FaceRecognizerManager :
    public QObject
{
    Q_OBJECT
public:

    const double CONFIDENCE_THRESHOLD = 1000;

    FaceRecognizerManager();
    ~FaceRecognizerManager();

    void addPath(QString path);
    void removePath(int index);
    void clearPaths();

    QList<QString>* getSamplePaths();
    QList<QString>* getLabels();
    void setLabelForPath(int pathPosition, QString label);

    bool trainOnSamples();
    bool trainFromFile(QString fileName);

    const RecognitionInfo recognizeFace(cv::Mat face);

    void saveTrainDataToFile(QString fileName);
    cv::Ptr<cv::FaceRecognizer> getFaceRecognizer();
    QString getNameForLabel(int label);
    bool isTrained();

private: 
    // Пути к каталогам с образцами
    QList<QString> samplePaths;
    // Строковые метки, соответствующие наборам образцов
    QList<QString> sampleLabels;
    // Объект, содержащий алгоритмы и модели для распознавания лиц
    cv::Ptr<cv::FaceRecognizer> mFaceRecognizer;
    // Флаг готовности к распознаванию: mFaceRecognizer успешно загрузил данные в модели.
    bool            trained;

    std::vector<cv::Mat> loadImagesFromPath(QString path, int height = 100, int width = 100);
};
