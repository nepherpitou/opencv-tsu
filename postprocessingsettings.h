#ifndef POSTPROCESSINGSETTINGS_H
#define POSTPROCESSINGSETTINGS_H

#include <QObject>
#include <QString>



class PostProcessingSettings : public QObject
{
    Q_OBJECT
public:
    explicit PostProcessingSettings(QObject *parent = 0);
    ~PostProcessingSettings();


    const QString SETTINGS_SAVE_RECOGNIZED = "SAVE_RECOGNIZED";
    const QString SETTINGS_SAVE_UNRECOGNIZED = "SAVE_UNRECOGNIZED";
    const QString SETTINGS_RECOGNIZED_PATH = "RECOGNIZED_PATH";
    const QString SETTINGS_UNRECOGNIZED_PATH = "UNRECOGNIZED_PATH";
    const QString SETTINGS_RECOGNIZED_INTERVAL = "RECOGNIZED_INTERVAL";
    const QString SETTINGS_UNRECOGNIZED_INTERVAL = "UNRECOGNIZED_INTERVAL";
    const QString SETTINGS_THRESHOLD = "THRESHOLD";

    void        setRecognizedFacesPolicy(bool save, QString path, int interval);
    void        setUnrecognizedFacesPolicy(bool save, QString path, int interval);
    void        setThreshold(int threshold);

    bool        saveRecognized();
    bool        saveUnrecognized();
    QString     getRecognizedPath();
    QString     getUnrecognizedPath();
    int         getRecInterval();
    int         getUnrecInterval();
    int         getThreshold();

    bool        loadSettingsFromFile(QString file);
    bool        saveSettingsToFile(QString file);

private:
    bool        saveRecognizedFaces;
    bool        saveUnrecognizedFaces;
    QString     recognizedPath;
    QString     unrecognizedPath;
    int         minimumRecSaveInterval;
    int         minimumUnrecSaveInterval;
    int         threshold;


signals:

public slots:
};

#endif // POSTPROCESSINGSETTINGS_H
