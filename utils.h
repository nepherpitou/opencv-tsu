#ifndef UTILS_H
#define UTILS_H

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <QDebug>
#include <QImage>

using namespace cv;

class Utils
{
public:
    Utils();

    static QImage mat2qimage(Mat inMat);

};

#endif // UTILS_H
