#-------------------------------------------------
#
# Project created by QtCreator 2014-09-30T23:41:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OpenCV_Detect_Peaple
TEMPLATE = app

QMAKE_CXXFLAGS += -std=gnu++0x

#LIBS += "D:\\Programming\\opencv_build\\install\\x64\\mingw\\lib\\\*.a"

#INCLUDEPATH += D:\\Programming\\opencv_build\\install\\include

LIBS += "I:/opencv/qt_build/install/x64/mingw/lib/*.a"

INCLUDEPATH += I:/opencv/qt_build/install/include


SOURCES += main.cpp\
        mainwindow.cpp \
    utils.cpp \
    searchfacesprocessor.cpp \
    searchfacesmanager.cpp \
    FaceDetectorManager.cpp \
    PostProcessor.cpp \
    postprocessingsettings.cpp \
    recognitioninfo.cpp

HEADERS  += mainwindow.h \
    utils.h \
    searchfacesprocessor.h \
    searchfacesmanager.h \
    FaceDetectorManager.h \
    PostProcessor.h \
    postprocessingsettings.h \
    recognitioninfo.h

FORMS    += mainwindow.ui
