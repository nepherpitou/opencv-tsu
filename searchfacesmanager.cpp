#include "searchfacesmanager.h"
#include <QFile>
#include <QDateTime>
#include <QTime>
#include <QDebug>
#include "recognitioninfo.h"

SearchFacesManager::SearchFacesManager(int maxThreads, QObject *parent) :
    QObject(parent),
    freeWorkers(maxThreads),
    threadsCount(0),
    maxThreadsCount(maxThreads),
    mFaceRecognizer(0)
{
}

void SearchFacesManager::processFrame(cv::Mat frame)
{
    if (freeWorkers.size() > 0)
        emit sendTask(frame, freeWorkers.takeFirst());
}

void SearchFacesManager::facesFound(std::vector<cv::Rect> faces, QList<cv::Mat> facesFrames)
{
    qDebug() << QString("Found %1 faces!\n").arg(faces.size());
    this->lastFoundFaces = faces;
    this->lastFoundFacesFrames = facesFrames;
    this->lastFacesFoundTime = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch();
    QList<RecognitionInfo> labels;
    for (uint i = 0; i < faces.size(); i++){
        if (mFaceRecognizer){
            labels.append(mFaceRecognizer->recognizeFace(facesFrames.at(i)));
        }
        else {
            RecognitionInfo info;
            info.label = faces.at(i).x;
            info.confidence = -1;
            info.face = facesFrames.at(i);
            labels.append(info);
        }
    }
    emit onFacesRecognizeFinished(faces, labels);
}


void SearchFacesManager::workerFinishWork(int elapsedTime, int workerNumber)
{
    qDebug() << QString("Worker %1 finished in %2ms").arg(workerNumber).arg(elapsedTime) << "\n";
    if (QDateTime::currentDateTimeUtc().toMSecsSinceEpoch() - this->lastFacesFoundTime > 500){
        this->lastFoundFaces.clear();
    }
    freeWorkers.append(workerNumber);
}


bool SearchFacesManager::startProcessing(QString haarClassifierPath)
{
    if (QFile(haarClassifierPath).exists()){
        this->haarCascadePath = haarClassifierPath;
        for (int i = 0; i < maxThreadsCount; i++){
            QThread* workerThread = new QThread();
            SearchFacesProcessor* worker = new SearchFacesProcessor(this->haarCascadePath, i);
            worker->moveToThread(workerThread);

            connect(workerThread, SIGNAL(started()), worker, SLOT(startProcessing()));
            connect(worker, SIGNAL(finished(int, int)), this, SLOT(workerFinishWork(int, int)));

            connect(worker,
                    SIGNAL(facesFound(std::vector<cv::Rect>, QList<cv::Mat>)),
                    this,
                    SLOT(facesFound(std::vector<cv::Rect>, QList<cv::Mat>)));

            connect(this, SIGNAL(sendTask(cv::Mat, int)), worker, SLOT(findFaces(cv::Mat, int)));

            connect(this, SIGNAL(stopAll()), workerThread, SLOT(quit()));
            workerThread->start();
            freeWorkers.append(i);
        }
        this->ready = true;
        return this->ready;
    }
    return false;
}

void SearchFacesManager::stopProcessing()
{
    emit stopAll();
    this->lastFoundFaces.clear();
    this->ready = false;
    this->threadsCount = 0;
}

QList<cv::Mat> SearchFacesManager::getLastFoundFacesFrames()
{
    return this->lastFoundFacesFrames;
}

void SearchFacesManager::setFaceRecognizer(FaceRecognizerManager * faceRecognizer)
{
    mFaceRecognizer = faceRecognizer;
}
