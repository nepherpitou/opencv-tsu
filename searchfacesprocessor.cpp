#include "searchfacesprocessor.h"
#include <QTime>

SearchFacesProcessor::SearchFacesProcessor(QString path, int number /*= 0*/, QObject *parent /*= 0*/) :
    QObject(parent),
    workerNumber(number),
    classifierPath(path)
{
}

void SearchFacesProcessor::startProcessing()
{
    this->classifier = new cv::CascadeClassifier(this->classifierPath.toStdString());
}

void SearchFacesProcessor::findFaces(cv::Mat frame, int workerNumber)
{
    if (this->workerNumber != workerNumber)
        return;
    QTime measurer;
    measurer.start();
    std::vector<cv::Rect>   foundFaces;
    QList<cv::Mat>    facesFrames;
    if(frame.cols > 0 && frame.rows > 0)
    {
        cv::cvtColor(frame, frame, CV_BGR2GRAY);
        this->classifier->detectMultiScale(frame, foundFaces, 1.05, 5, CV_HAAR_SCALE_IMAGE, cv::Size(100, 100));
        for(uint i = 0; i < foundFaces.size(); i++)
        {
            cv::Rect face = foundFaces.at(i);
            cv::Mat faceMat(frame, face);
            facesFrames.append(faceMat);
        }
        emit facesFound(foundFaces, facesFrames);
    }
    emit finished(measurer.elapsed(), this->workerNumber);
}


