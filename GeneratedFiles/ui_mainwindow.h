/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_2;
    QComboBox *sourceSelect;
    QPushButton *pbStart;
    QPushButton *pbStop;
    QLabel *lbFrame;
    QWidget *tab_2;
    QGridLayout *gridLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *clearSamples;
    QPushButton *addSample;
    QPushButton *removeSample;
    QSpacerItem *verticalSpacer;
    QPushButton *trainSamples;
    QListWidget *samplesList;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(769, 522);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout_2 = new QGridLayout(tab);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        sourceSelect = new QComboBox(tab);
        sourceSelect->setObjectName(QStringLiteral("sourceSelect"));

        gridLayout_2->addWidget(sourceSelect, 1, 0, 1, 1);

        pbStart = new QPushButton(tab);
        pbStart->setObjectName(QStringLiteral("pbStart"));

        gridLayout_2->addWidget(pbStart, 1, 1, 1, 1);

        pbStop = new QPushButton(tab);
        pbStop->setObjectName(QStringLiteral("pbStop"));

        gridLayout_2->addWidget(pbStop, 1, 2, 1, 1);

        lbFrame = new QLabel(tab);
        lbFrame->setObjectName(QStringLiteral("lbFrame"));
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbFrame->sizePolicy().hasHeightForWidth());
        lbFrame->setSizePolicy(sizePolicy);
        lbFrame->setScaledContents(true);

        gridLayout_2->addWidget(lbFrame, 0, 0, 1, 3);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_3 = new QGridLayout(tab_2);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 4, 1, 1, 1);

        clearSamples = new QPushButton(tab_2);
        clearSamples->setObjectName(QStringLiteral("clearSamples"));

        gridLayout_3->addWidget(clearSamples, 3, 1, 1, 1);

        addSample = new QPushButton(tab_2);
        addSample->setObjectName(QStringLiteral("addSample"));

        gridLayout_3->addWidget(addSample, 1, 1, 1, 1);

        removeSample = new QPushButton(tab_2);
        removeSample->setObjectName(QStringLiteral("removeSample"));

        gridLayout_3->addWidget(removeSample, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer, 5, 1, 1, 1);

        trainSamples = new QPushButton(tab_2);
        trainSamples->setObjectName(QStringLiteral("trainSamples"));

        gridLayout_3->addWidget(trainSamples, 6, 1, 1, 1);

        samplesList = new QListWidget(tab_2);
        samplesList->setObjectName(QStringLiteral("samplesList"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(samplesList->sizePolicy().hasHeightForWidth());
        samplesList->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(samplesList, 0, 0, 7, 1);

        tabWidget->addTab(tab_2, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 769, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);
        QObject::connect(sourceSelect, SIGNAL(currentIndexChanged(int)), MainWindow, SLOT(onSourceChanged(int)));
        QObject::connect(pbStart, SIGNAL(clicked()), MainWindow, SLOT(onStartClicked()));
        QObject::connect(pbStop, SIGNAL(clicked()), MainWindow, SLOT(onStopClicked()));
        QObject::connect(addSample, SIGNAL(clicked()), MainWindow, SLOT(onAddSampleClicked()));
        QObject::connect(trainSamples, SIGNAL(clicked()), MainWindow, SLOT(onTrainSamplesClicked()));

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        sourceSelect->clear();
        sourceSelect->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Camera", 0)
         << QApplication::translate("MainWindow", "Video File", 0)
         << QApplication::translate("MainWindow", "Picture", 0)
        );
        pbStart->setText(QApplication::translate("MainWindow", "Start Capture", 0));
        pbStop->setText(QApplication::translate("MainWindow", "Stop Capture", 0));
        lbFrame->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Capture", 0));
        clearSamples->setText(QApplication::translate("MainWindow", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", 0));
        addSample->setText(QApplication::translate("MainWindow", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0));
        removeSample->setText(QApplication::translate("MainWindow", "\320\243\320\261\321\200\320\260\321\202\321\214", 0));
        trainSamples->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Recog. Settings", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
