#ifndef RECOGNITIONINFO_H
#define RECOGNITIONINFO_H

#include <QObject>
#include <QTime>
#include "opencv2/core/core.hpp"
class RecognitionInfo
{
public:
    explicit RecognitionInfo();
    ~RecognitionInfo();
    QString         label;
    double          confidence;
    QTime           time;
    cv::Mat         face;
    bool            recognized;

signals:

public slots:
};

#endif // RECOGNITIONINFO_H
